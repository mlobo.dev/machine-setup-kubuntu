!/bin/bash
echo Atualizando repositórios..

if ! apt-get update; then
    echo "Não foi possível atualizar os repositórios. Verifique seu arquivo /etc/apt/sources.list"
    exit 1
fi
echo "Atualização feita com sucesso"

echo "Atualizando pacotes já instalados"
if ! apt-get dist-upgrade -y; then
    echo "Não foi possível atualizar pacotes."
    exit 1
fi
echo "Atualização de pacotes feita com sucesso"

echo "Intalando openJdk11"

if ! apt-get install openjdk-11-jdk -y; then
    echo "não foi possível install openjdk-11"
    exit 1
fi
echo " openJdk11 instalado com sucesso"

echo "Intalando openJdk8"

if ! apt-get install openjdk-8-jdk -y; then
    echo "não foi possível install openjdk-8"
    exit 1
fi
echo " openjdk-8-jdk instalado com sucesso"

echo "Intalando nodeJS e npm"

curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh
sudo bash nodesource_setup.sh

if ! apt install nodejs -y && apt install npm -y; then
    echo "não foi possível install nodejs"
    exit 1
fi
echo " node js e npm instalado com sucesso"

echo "instalando yarn "
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
sudo apt update
sudo apt install yarn -y
echo " yarn instalado com sucesso "

echo "Intalando pacotes doDocker"

if ! apt-get install \
    apt-transport-https -y \
    ca-certificates -y \
    curl -y gnupg -y lsb-release -y; then
    echo "não foi possível instalar pacotes  docker"
    exit 1
fi

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg -y
echo \
    "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list >/dev/null

sudo apt-get update -y

if ! apt-get install docker-ce docker-ce-cli containerd.io -y; then
    echo "não foi possível docker"
    exit 1
fi
echo "docker instalado com sucesso"

echo "instalando docker composer"
if ! apt install docker-compose -y; then
    echo "não foi possível docker"
    exit 1
fi
echo "docker composer instalado com sucesso"

echo "instalando intellij IDEA"
if ! snap install intellij-idea-community --classic; then
    echo "não foi possível instalar intellij"
    exit 1
fi
echo "intellij IDEA instalado com sucesso"

echo "instalando Android Studio"
if ! snap install android-studio --classic; then
    echo "não foi possível instalar Android Studio"
    exit 1
fi
echo "Android Studio instalado com sucesso"

echo "instalando Dbeaver"
if ! snap install dbeaver-ce; then
    echo "não foi possível instalar Dbeaver"
    exit 1
fi
echo "Dbeaver instalado com sucesso"

echo "instalando VS Code"
if ! sudo snap install code --classic; then
    echo "não foi possível instalar VS Code"
    exit 1
fi
echo "VS Code instalado com sucesso"

echo "instalando Flameshot"
if ! sudo snap install flameshot; then
    echo "não foi possível instalar Flameshot"
    exit 1
fi
echo "Flameshot instalado com sucesso"

echo "instalando Postman"
if ! sudo snap install postman; then
    echo "não foi possível instalar Postman"
    exit 1
fi
echo "Postman instalado com sucesso"

echo "instalando obs-studio"
if ! sudo snap install obs-studio; then
    echo "não foi possível instalar obs-studio"
    exit 1
fi
echo "Obs Studio instalado com sucesso"

echo "instalando teams"
if ! sudo snap install teams-for-linux; then
    echo "não foi possível instalar teams"
    exit 1
fi
echo "teams instalado com sucesso"

echo "instalando Maven"
if ! sudo apt install maven -y; then
    echo "não foi possível instalar Maven"
    exit 1
fi
echo "Maven instalado com sucesso"

echo "instalando mongo compass"
wget https://downloads.mongodb.com/compass/mongodb-compass_1.28.4_amd64.deb
if ! sudo apt install ./mongodb-compass_1.28.4_amd64.deb -y; then
    echo "não foi possível instalar mongo compass"
    exit 1
fi
echo "Mongo compass instalado com sucesso"

echo "instalando Git Fiend"
wget https://gitfiend.com/resources/GitFiend_0.29.0_amd64.deb
if ! sudo apt install ./GitFiend_0.29.0_amd64.deb -y; then
    echo "não foi possível instalar Git Fiend"
    exit 1
fi
echo "Git Fiend instalado com sucesso"


echo "instalando Chrome"
wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
sudo apt-get update
if ! sudo apt install google-chrome-stable; then
    echo "não foi possível instalar Chrome"
    exit 1
fi
echo "Chrome instalado com sucesso"

echo "instalando  banco de dados com docker...."

echo "Mysql..."
sudo docker-compose -f ./docker/mysql.yml up --remove-orphans -d
echo "Mysql instanciado com sucesso!"

echo "MariaDB"
sudo docker-compose -f ./docker/mariadb.yml up --remove-orphans -d
echo "MariaDB instanciado com sucesso!"

echo "Postgres..."
sudo docker-compose -f ./docker/postgres.yml up --remove-orphans -d
echo "Postgres instanciado com sucesso!"

echo "Mongo..."
sudo docker-compose -f ./docker/mongodb.yml up --remove-orphans -d
echo "Mongo instanciado com sucesso!"

echo "Removendo instaladores"
rm -r mongodb-compass_1.28.4_amd64.deb
rm -r nodesource_setup.sh
rm -r GitFiend_0.29.0_amd64.deb